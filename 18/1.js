// 尝试打开名为 "MyDatabase" 的数据库，如果不存在则创建之，版本号为 1。
var request = indexedDB.open("MyDatabase", 1);

// 当需要升级数据库时触发此事件（首次创建或版本号增加）。
request.onupgradeneeded = function(event) {
    // 获取数据库实例
    var db = event.target.result;

    // 创建一个名为 "books" 的对象存储空间，并指定 "isbn" 作为主键。
    // 每个存储在 "books" 中的对象都必须有一个名为 "isbn" 的属性作为其唯一标识符。
    var objectStore = db.createObjectStore("books", { keyPath: "isbn" });

    // 可选地，在这里可以为 "books" 对象存储创建索引，以加速特定字段的查询。
    // 例如：objectStore.createIndex("titleIndex", "title", { unique: false });
};

// 数据库成功打开时触发此事件。
request.onsuccess = function(event) {
    // 保存对数据库连接的引用（在这个简单的例子中未使用）
    var db = event.target.result;

    // 输出确认信息到控制台，表示数据库已经成功打开。
    console.log("数据库已成功打开");

    // 注意：在此之后，你通常会开始进行事务操作（如添加、读取、更新或删除数据）。
};

// 如果打开数据库过程中出现错误，则触发此事件。
request.onerror = function(event) {
    // 打印错误信息到控制台，帮助调试问题。
    console.error("数据库打开失败");
};