function deleteBook(db, isbn) {
    // 开始一个新事务，指定要访问的对象存储空间（"books"），并设置为读写模式 ("readwrite")。
    // 由于我们打算删除数据，因此需要使用 "readwrite" 模式以允许对数据库进行更改。
    var transaction = db.transaction(["books"], "readwrite");

    // 从当前事务中获取名为 "books" 的对象存储空间实例。
    var objectStore = transaction.objectStore("books");

    // 使用 delete() 方法尝试根据提供的 ISBN 删除书本记录。
    // 如果对象存储中存在具有相同主键（这里是指定的 "isbn" 字段）的记录，则该记录会被删除；
    // 如果不存在这样的记录，则不会有任何操作执行，但也不会抛出错误。
    var deleteRequest = objectStore.delete(isbn);

    // 当 delete() 操作成功完成时触发此事件处理程序。
    // 注意：即使没有找到对应的记录，onsuccess 也会被触发，因为 delete() 成功意味着请求本身已成功执行。
    deleteRequest.onsuccess = function(event) {
        // 输出确认信息到控制台，表示书本已成功删除。
        console.log("书本删除成功");
    };

    // 当整个事务完成时触发此事件处理程序，无论是否有任何操作失败。
    // 这是确保所有操作都已完成的最后一个检查点。
    transaction.oncomplete = function() {
        // 输出信息到控制台，表示所有事务内的操作已完成。
        console.log("事务完成");
    };

    // 如果事务中的任何一个操作发生错误，则触发此事件处理程序。
    // 错误可能由多种原因引起，如违反约束、网络问题等。
    transaction.onerror = function(event) {
        // 输出错误信息到控制台，帮助调试问题。
        console.error("删除失败", event.target.error);
    };
}

// 使用方法：尝试从数据库中删除一本书。
// 注意：这里的 "db" 参数应该是由 indexedDB.open 成功返回的数据库连接对象，而不是字符串 "MyDatabase"。
// 正确的做法是先打开数据库，然后使用返回的数据库连接对象调用 deleteBook 函数。
var request = indexedDB.open("MyDatabase", 1);

request.onsuccess = function(event) {
    // 获取数据库连接
    var db = event.target.result;

    // 现在可以安全地调用 deleteBook 函数，并传递实际的数据库连接对象。
    deleteBook(db, '123456789');

    // 关闭数据库连接（通常在所有操作完成后进行）
    // db.close();
};

request.onerror = function(event) {
    // 如果打开数据库过程中出现错误，则输出错误信息。
    console.error("数据库打开失败", event.target.error);
};