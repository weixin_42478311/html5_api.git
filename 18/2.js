function addBook(db, book) {
    // 开始一个新事务，指定要访问的对象存储空间（"books"），并设置为读写模式 ("readwrite")。
    var transaction = db.transaction(["books"], "readwrite");

    // 从当前事务中获取名为 "books" 的对象存储空间实例。
    var objectStore = transaction.objectStore("books");

    // 向对象存储空间中添加一个新的书本记录。如果主键（这里是指定的 "isbn" 字段）已经存在，则会抛出错误。
    var addRequest = objectStore.add(book);

    // 当添加操作成功完成时触发此事件处理程序。
    addRequest.onsuccess = function(event) {
        // 输出确认信息到控制台，表示书本已成功添加。
        console.log("书本添加成功");
    };

    // 当整个事务完成时触发此事件处理程序，无论是否有任何操作失败。
    transaction.oncomplete = function() {
        // 输出信息到控制台，表示所有事务内的操作已完成。
        console.log("事务完成");
    };

    // 如果事务中的任何一个操作发生错误，则触发此事件处理程序。
    transaction.onerror = function(event) {
        // 输出错误信息到控制台，帮助调试问题。
        console.error("添加失败");
    };
}

// 使用方法：尝试向数据库中添加一本书。
// 注意：这里的 "db" 参数应该是由 indexedDB.open 成功返回的数据库连接对象，而不是字符串 "MyDatabase"。
// 正确的做法是先打开数据库，然后使用返回的数据库连接对象调用 addBook 函数。
var request = indexedDB.open("MyDatabase", 1);

request.onsuccess = function(event) {
    // 获取数据库连接
    var db = event.target.result;

    // 现在可以安全地调用 addBook 函数，并传递实际的数据库连接对象。
    addBook(db, { isbn: '123456789', title: 'HTML5入门' });
    // addBook(db, { isbn: '12345', title: 'go入门' });

    // 关闭数据库连接（通常在所有操作完成后进行）
    // db.close();
};

request.onerror = function(event) {
    console.error("数据库打开失败");
};