function getBook(db, isbn) {
    // 开始一个新事务，指定要访问的对象存储空间（"books"），并设置为只读模式 ("readonly")。
    // 由于我们只是查询数据而不会修改它，所以使用 "readonly" 是合适的。
    var transaction = db.transaction(["books"], "readonly");

    // 从当前事务中获取名为 "books" 的对象存储空间实例。
    var objectStore = transaction.objectStore("books");

    // 尝试根据提供的 ISBN 获取书本记录。
    var getRequest = objectStore.get(isbn);

    // 当获取操作成功完成时触发此事件处理程序。
    getRequest.onsuccess = function(event) {
        // 检查是否找到了匹配的书本记录。
        if (event.target.result) {
            // 如果找到，输出书本信息到控制台。
            console.log("找到的书本:", event.target.result);
        } else {
            // 如果没有找到对应的 ISBN，输出未找到的信息。
            console.log("未找到该ISBN的书本");
        }
    };

    // 注意：对于只读事务，通常不需要监听 oncomplete 或 onerror 事件，
    // 因为这些事件主要用于确保所有写入操作都已成功完成或用于错误处理。
}

// 使用方法：尝试从数据库中获取一本书。
// 注意：这里的 "db" 参数应该是由 indexedDB.open 成功返回的数据库连接对象，而不是字符串 "MyDatabase"。
// 正确的做法是先打开数据库，然后使用返回的数据库连接对象调用 getBook 函数。
var request = indexedDB.open("MyDatabase", 1);

request.onsuccess = function(event) {
    // 获取数据库连接
    var db = event.target.result;

    // 现在可以安全地调用 getBook 函数，并传递实际的数据库连接对象。
    getBook(db, '123456789');

    // 关闭数据库连接（通常在所有操作完成后进行）
    // db.close();
};

request.onerror = function(event) {
    console.error("数据库打开失败");
};