function listAllBooks(db) {
    // 开始一个新事务，指定要访问的对象存储空间（"books"），并设置为只读模式 ("readonly")。
    // 由于我们只是遍历数据而不会修改它，所以使用 "readonly" 是合适的。
    var transaction = db.transaction(["books"], "readonly");

    // 从当前事务中获取名为 "books" 的对象存储空间实例。
    var objectStore = transaction.objectStore("books");

    // 使用 openCursor() 方法创建一个游标来遍历对象存储中的所有记录。
    // 游标允许我们逐条访问对象存储中的记录。
    objectStore.openCursor().onsuccess = function(event) {
        // 获取游标对象
        var cursor = event.target.result;

        if (cursor) {
            // 如果存在当前记录，则输出书本信息到控制台。
            console.log("书名:", cursor.value.title, ", ISBN:", cursor.value.isbn);

            // 继续移动游标到下一个记录。
            cursor.continue();
        } else {
            // 如果没有更多记录，输出遍历完毕的信息。
            console.log("遍历完毕");
        }
    };
}

// 使用方法：尝试列出数据库中的所有书籍。
// 注意：这里的 "db" 参数应该是由 indexedDB.open 成功返回的数据库连接对象，而不是字符串 "MyDatabase"。
// 正确的做法是先打开数据库，然后使用返回的数据库连接对象调用 listAllBooks 函数。
var request = indexedDB.open("MyDatabase", 1);

request.onsuccess = function(event) {
    // 获取数据库连接
    var db = event.target.result;

    // 现在可以安全地调用 listAllBooks 函数，并传递实际的数据库连接对象。
    listAllBooks(db);

    // 关闭数据库连接（通常在所有操作完成后进行）
    // db.close();
};

request.onerror = function(event) {
    // 如果打开数据库过程中出现错误，则输出错误信息。
    console.error("数据库打开失败", event.target.error);
};