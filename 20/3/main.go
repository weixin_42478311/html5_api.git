package main

import (
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

// getWeatherData 获取天气数据。在实际应用中，这个函数应该与天气服务API交互以获取实时数据。
// 返回值: 一个描述天气的字符串。
func getWeatherData() string {
	// 这里应该有一个函数来获取实际的天气数据
	return "Sunny with a chance of meatballs."
}

// main 是程序的入口点。它设置了一个Gin路由器，配置了两个GET请求的处理器，并启动了服务器。
func main() {
	r := gin.Default()
	r.LoadHTMLGlob("templates/*")

	// 定义根路径"/"的GET请求处理器。
	// 当用户访问根URL时，将渲染并返回"index.html"模板。
	r.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", nil)
	})

	// 定义"/weather"路径的GET请求处理器。
	// 该处理器通过Server-Sent Events (SSE)每5分钟向客户端发送一次天气更新。
	r.GET("/weather", func(c *gin.Context) {
		c.Header("Content-Type", "text/event-stream")
		c.Header("Cache-Control", "no-cache")
		c.Header("Connection", "keep-alive")

		flusher, ok := c.Writer.(http.Flusher)
		if !ok {
			http.Error(c.Writer, "Streaming unsupported!", http.StatusInternalServerError)
			return
		}

		for {
			data := getWeatherData()
			c.Writer.WriteString(fmt.Sprintf("data: %s\n\n", data))
			flusher.Flush()

			time.Sleep(5 * time.Minute) // 每五分钟更新一次

			if c.Writer.Size() == 0 {
				break
			}
		}
	})

	// 启动HTTP服务器，监听8080端口。
	r.Run(":8080")
}
