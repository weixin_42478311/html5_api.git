package main

import (
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()

	// 加载HTML模板文件夹中的所有HTML文件，以便可以在响应中使用这些模板。
	// 这里假设HTML文件位于项目根目录下的"templates"文件夹中。
	r.LoadHTMLGlob("templates/*")

	// 定义根路径"/"的GET请求处理器。
	// 当用户访问根URL时，将渲染并返回"index.html"模板。
	r.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", nil)
	})

	// 定义处理SSE事件的GET请求处理器，路径为"/events"。
	r.GET("/events", func(c *gin.Context) {
		// 设置HTTP响应头以支持SSE。
		// Content-Type设置为"text/event-stream"表示这是一个SSE流。
		// Cache-Control设置为"no-cache"防止浏览器缓存数据。
		// Connection设置为"keep-alive"保持连接打开。
		c.Header("Content-Type", "text/event-stream")
		c.Header("Cache-Control", "no-cache")
		c.Header("Connection", "keep-alive")

		// 检查ResponseWriter是否实现了Flusher接口，确保可以实时发送数据。
		flusher, ok := c.Writer.(http.Flusher)
		if !ok {
			http.Error(c.Writer, "Streaming unsupported!", http.StatusInternalServerError)
			return
		}

		// 开始一个循环来模拟服务器向客户端发送20条消息。
		for i := 0; i < 20; i++ { // 发送20条消息
			// 构建要发送的消息，格式为"data: [message]\n\n"。
			message := fmt.Sprintf("data: %d\n\n", i)
			c.Writer.WriteString(message)

			// 调用Flush()方法立即将缓冲区的数据发送给客户端。
			flusher.Flush()

			// 每次发送消息后暂停1秒，模拟延迟。
			time.Sleep(time.Second)

			// 如果客户端断开了连接（例如关闭了页面），则退出循环。
			if c.Writer.Size() == 0 {
				break
			}
		}
	})

	// 启动HTTP服务器，监听8080端口。
	// Gin框架默认在0.0.0.0上监听，意味着可以从任何网络接口访问该服务。
	r.Run(":8080") // 监听并在 0.0.0.0:8080 上启动服务
}
