package main

import (
	"fmt"
	"math/rand"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

// getRandomStockPrice 生成一个随机的股票价格。
// 假设股票价格在100到200之间。
func getRandomStockPrice() float64 {
	return rand.Float64()*100 + 100
}

func main() {
	r := gin.Default()
	r.LoadHTMLGlob("templates/*")

	// 定义根路径"/"的GET请求处理器。
	// 当用户访问根URL时，将渲染并返回"index.html"模板。
	r.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", nil)
	})

	// 处理"/stock"的GET请求，通过Server-Sent Events (SSE) 实时推送股票价格。
	r.GET("/stock", func(c *gin.Context) {
		c.Header("Content-Type", "text/event-stream")
		c.Header("Cache-Control", "no-cache")
		c.Header("Connection", "keep-alive")

		// 检查响应写入器是否支持flush操作。
		flusher, ok := c.Writer.(http.Flusher)
		if !ok {
			http.Error(c.Writer, "Streaming unsupported!", http.StatusInternalServerError)
			return
		}

		// 创建一个定时器，每10秒发送一次股票价格。
		ticker := time.NewTicker(10 * time.Second)
		defer ticker.Stop()

		for {
			select {
			case <-ticker.C:
				// 生成新的股票价格并发送到客户端。
				price := getRandomStockPrice()
				c.Writer.WriteString(fmt.Sprintf("data: %.2f\n\n", price))
				flusher.Flush()

				// 如果响应内容为空，则终止循环。
				if c.Writer.Size() == 0 {
					return
				}
			}
		}

	})

	// 启动HTTP服务器，监听端口8080。
	r.Run(":8080")
}
