package main

import (
	"fmt"
	"net/http"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
)

// messages 存储聊天消息。
var messages []string

// mu 用于保护 messages 切片的并发访问安全。
var mu sync.Mutex

// addMessage 向消息列表中添加新消息。
// 该函数通过 mu 锁确保并发安全。
func addMessage(msg string) {
	mu.Lock()
	defer mu.Unlock()
	messages = append(messages, msg)
}

func main() {
	r := gin.Default()
	r.LoadHTMLGlob("templates/*")

	// 定义根路径"/"的GET请求处理器。
	// 当用户访问根URL时，将渲染并返回"index.html"模板。
	r.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", nil)
	})

	// 处理"/chat"的GET请求，以Server-Sent Events(SSE)形式发送聊天消息。
	// 该处理器持续运行，每秒向客户端推送一次消息，直到连接关闭。
	r.GET("/chat", func(c *gin.Context) {
		c.Header("Content-Type", "text/event-stream")
		c.Header("Cache-Control", "no-cache")
		c.Header("Connection", "keep-alive")

		flusher, ok := c.Writer.(http.Flusher)
		if !ok {
			http.Error(c.Writer, "Streaming unsupported!", http.StatusInternalServerError)
			return
		}

		for {
			mu.Lock()
			msgs := messages
			mu.Unlock()

			for _, msg := range msgs {
				c.Writer.WriteString(fmt.Sprintf("data: %s\n\n", msg))
				flusher.Flush()
			}

			time.Sleep(time.Second)

			if c.Writer.Size() == 0 {
				break
			}
		}
	})

	// 处理"/send"的POST请求，用于接收客户端发送的新消息。
	// 从请求中提取"message"字段，并将其添加到消息列表中。
	r.POST("/send", func(c *gin.Context) {
		msg := c.PostForm("message")
		addMessage(msg)
	})

	// 启动HTTP服务器，监听端口为8080。
	r.Run(":8080")
}
