/**
 * 当Worker接收到消息时，此事件处理函数会被触发
 * 它对接收到的数据进行排序，并将排序后的结果发回
 * 
 * @param {MessageEvent} event - 事件对象，包含从主线程发送过来的数据
 */
self.onmessage = function(event) {
    // 从事件对象中获取传递过来的数组
    let array = event.data;

    // 对数组进行排序，确保数组中的数字按升序排列
    array.sort((a, b) => a - b);

    // 将排序后的数组发回主线程
    self.postMessage(array);
};