// 每秒发送当前时间的时分秒表示，作为时间服务工作者的主循环
setInterval(function() {
    // 创建一个日期对象，并使用toLocaleTimeString方法获取当前时间的时分秒表示
    // self.postMessage用于向工作者的监听者发送消息，在这里发送的是当前时间
    self.postMessage(new Date().toLocaleTimeString());
}, 1000);