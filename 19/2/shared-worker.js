var clients = [];

self.onconnect = function(e) {
    var port = e.ports[0];
    clients.push(port);
    
    port.onmessage = function(event) {
        let action = event.data;
        if (action === 'increment') {
            for (let client of clients) {
                client.postMessage('Counter incremented');
            }
        }
    };

    port.start();
};