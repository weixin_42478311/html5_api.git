self.onmessage = function(event) {
    let data = event.data;
    console.log('Received:', data);

    // 计算平方根
    let result = Math.sqrt(data);
    
    // 发送结果回主线程
    self.postMessage(result);
};